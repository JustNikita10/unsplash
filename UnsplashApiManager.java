package nik.unsplash.android.unsplashclient.request;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import nik.unsplash.android.unsplashclient.utils.Tls12SocketFactory;
import nik.unsplash.android.unsplashclient.callback.DataDownloaded;
import nik.unsplash.android.unsplashclient.models.SearchResult;
import nik.unsplash.android.unsplashclient.models.UnsplashModel;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static nik.unsplash.android.unsplashclient.utils.Constants.APP_ID;
import static nik.unsplash.android.unsplashclient.utils.Constants.BASE_URL;

/**
 * Created by dell on 15.05.2018.
 */

public class UnsplashApiManager {
    private UnsplashApi unsplashApi;

    public UnsplashApiManager() {
        init();
    }

    public void init() {
        Retrofit retrofit = new Retrofit.Builder()
                .client(getNewHttpClient())
                .baseUrl(BASE_URL) //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        unsplashApi = retrofit.create(UnsplashApi.class);
    }

    private OkHttpClient getNewHttpClient() {
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .cache(null)
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS);
        return enableTls12OnPreLollipop(client).build();
    }

    public static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                client.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();

                List specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);

                client.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
            }
        }

        return client;
    }

    public void getPhotos(Integer page, Integer perPage, final DataDownloaded callback){
        unsplashApi.getPhotos(APP_ID, page,perPage).enqueue(new Callback<List<UnsplashModel>>() {
            @Override
            public void onResponse(Call<List<UnsplashModel>> call, Response<List<UnsplashModel>> response) {
                if(response != null && response.body().size() > 0) {
                    System.out.println("Data got");
                    callback.onComplete(response.body());
                }
                else {
                    System.out.println("No data got");
                    callback.onError("No data got");
                }
            }

            @Override
            public void onFailure(Call<List<UnsplashModel>> call, Throwable t) {
                System.out.println(t.getMessage());
                callback.onError("Error while downloading");
            }
        });
    }

    public void searchPhotos(@NonNull String query, @Nullable Integer page, @Nullable Integer perPage, final DataDownloaded callback) {
        unsplashApi.searchPhotos(APP_ID,query, page, perPage).enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                if(response != null && response.body().getResults().size() > 0) {
                    System.out.println("Data got");
                    callback.onSearchComplete(response.body());
                }
                else {
                    System.out.println("No data got");
                    callback.onError("No data got");
                }
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {
                System.out.println(t.getMessage());

                callback.onError("Error while downloading");
            }
        });
    }
}
