package nik.unsplash.android.unsplashclient.request;

import java.util.List;

import nik.unsplash.android.unsplashclient.models.SearchResult;
import nik.unsplash.android.unsplashclient.models.UnsplashModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by dell on 13.05.2018.
 */

public interface UnsplashApi {
    @GET("photos")
    Call<List<UnsplashModel>> getPhotos(@Query("client_id") String client_id);

    @GET("photos")
    Call<List<UnsplashModel>> getPhotos(@Query("client_id") String t,@Query("page") Integer page, @Query("per_page") Integer perPage);

    @GET("search/photos")
    Call<SearchResult> searchPhotos(@Query("client_id") String t, @Query("query") String query, @Query("page") Integer page, @Query("per_page") Integer perPage);
}
